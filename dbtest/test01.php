<!DOCTYPE html>
<?php
  $DB_DSN = "mysql:host=localhost; dbname=hyokota; charset=utf8";
  $DB_USER = "webaccess";
  $DB_PW = "toMeu4rH";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);

  $query_str = "SELECT *
                FROM test_table01";

  echo $query_str;
  $sql = $pdo->prepare($query_str);
  $sql->execute();
  $result = $sql->fetchAll();

 ?>
<html>
  <head>
    <meta charset=utf-8>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylsheet" href="./include/style.css">
    <title>第7回：MySQL</title>
  </head>
  <body>

    <pre>
    <?php var_dump($result); ?>
    </pre>

    <?php // あとでテーブル追加
    foreach ($result as $each)
    {
      echo $each['dish_name'] . " : " . $each['price'] . " 円 ";
      echo "<hr/>";
    }
    ?>

    <table border='1'>
      <tr>
        <th>ID</th>
        <th>料理名</th>
        <th>ジャンル</th>
        <th>値段</th>
        <th>メモ</th>
      <?php
      foreach ($result as $each)
      {
        echo
          "<tr>"
          . "<td>" . $each['ID'] . "</td>"
          . "<td>" . $each['dish_name'] . "</td>"
          . "<td>" . $each['genre'] . "</td>"
          . "<td>" . $each['price'] . "</td>"
          . "<td>" . $each['memo'] . "</td>"
          . "</tr>" ;
      }
      ?>
      </tr>
    </table>
  </body>
</html>
