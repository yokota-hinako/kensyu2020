<!-- entry_update02とほぼ同じ処理だけど、insirt intoで構築するよ～ -->

<!DOCTYPE html>

  <!-- 生データ取得 -->
  <pre>
  <?php  var_dump($_POST); ?>
  </pre>

  <?php
    include("./include/statics.php");//都道府県と性別の呼び出し
    include("./include/functions.php");//エラー処理の設定

    // パラメーターチェック開始
    $param_namae = "";
    if (isset($_POST['new_namae']) AND $_POST['new_namae'] !="")
    {
      $param_namae = $_POST['new_namae'];
    }
    else
    {
      commonError();
    }

    $param_pref = "";
    if (isset($_POST['new_pref']) AND $_POST['new_pref'] !="")
    {
      $param_pref = $_POST['new_pref'];
    }
    else
    {
      commonError();
    }

    $param_sex = "";
    if (isset($_POST['new_sex']) AND $_POST['new_sex'] !="")
    {
      $param_sex = $_POST['new_sex'];
    }
    else
    {
      commonError();
    }

    $param_age = "";
    if (isset($_POST['new_age']) AND $_POST['new_age'] !="")
    {
      $param_age = $_POST['new_age'];
    }
    else
    {
      commonError();
    }

    $param_section = "";
    if (isset($_POST['new_section']) AND $_POST['new_section'] !="")
    {
      $param_section = $_POST['new_section'];
    }
    else
    {
      commonError();
    }

    $param_grade = "";
    if (isset($_POST['new_grade']) AND $_POST['new_grade'] !="")
    {
      $param_grade = $_POST['new_grade'];
    }
    else
    {
      commonError();
    }

    $pdo = commonDB();//改めてDB呼び出し

    // 登録を実行するための命令文(新規追加なのでINSERT INTO)
    //member_IDはAIで生成するため、ここではNULLを入れる
    $query_str = "INSERT INTO member (member_ID, name, pref, seibetu, age, section_ID, grade_ID)
                  VALUES
                  (
                    NULL,
                    '" . $param_namae . "',
                    '" . $param_pref . "',
                    '" . $param_sex . "',
                    '" . $param_age . "',
                    '" . $param_section . "',
                    '" . $param_grade . "'
                  )";

    try
    {
      $sql = $pdo -> prepare($query_str);
      $sql -> execute();
      $id = $pdo -> lastInsertID('member_ID');//ここでAIで生成した値を取ってくる
    }
    catch (PDOException $e)
    {
      print $e -> getMessage();
    }

    echo $query_str;

    $url = "./detail01.php?id=" . $id;//URLの末尾につくmember_IDは、$idで定義した値を使うよ～という意味のコード
    header('Location: ' . $url);
    exit;

  ?>
