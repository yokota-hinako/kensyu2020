<!DOCTYPE html>
  <!-- 生データ取得 -->
  <pre>
  <?php  var_dump($_POST); ?>
  </pre>

  <?php
    include("./include/statics.php");//都道府県と性別の呼び出し
    include("./include/functions.php");//エラー処理・DBの指定

  // パラメーターチェック
    $param_id = "";
    if (isset($_POST['member_ID']) AND $_POST['member_ID'] !="")
    {
      $param_id = $_POST['member_ID'];
    }
    else
    {
      commonError();
    }

    $param_namae = "";
    if (isset($_POST['up_namae']) AND $_POST['up_namae'] !="")
    {
      $param_namae = $_POST['up_namae'];
    }
    else
    {
      commonError();
    }

    $param_pref = "";
    if (isset($_POST['up_pref']) AND $_POST['up_pref'] !="")
    {
      $param_pref = $_POST['up_pref'];
    }
    else
    {
      commonError();
    }

    $param_sex = "";
    if (isset($_POST['up_sex']) AND $_POST['up_sex'] !="")
    {
      $param_sex = $_POST['up_sex'];
    }
    else
    {
      commonError();
    }

    $param_age = "";
    if (isset($_POST['up_age']) AND $_POST['up_age'] !="")
    {
      $param_age = $_POST['up_age'];
    }
    else
    {
      commonError();
    }

    $param_section = "";
    if (isset($_POST['up_section']) AND $_POST['up_section'] !="")
    {
      $param_section = $_POST['up_section'];
    }
    else
    {
      commonError();
    }

    $param_grade = "";
    if (isset($_POST['up_grade']) AND $_POST['up_grade'] !="")
    {
      $param_grade = $_POST['up_grade'];
    }
    else
    {
      commonError();
    }
  // パラメーターチェック終了

    $pdo = commonDB();//改めてDB呼び出し

  // 登録を実行するための命令文
    $query_str = "UPDATE member AS m
                  SET m.name = '" . $param_namae . "',
                      m.pref = '" . $param_pref . "',
                      m.seibetu = '" . $param_sex . "',
                      m.age = '" . $param_age . "',
                      m.section_ID = '" . $param_section . "',
                      m.grade_ID = '" . $param_grade . "'
                  WHERE m.member_ID = " . $param_id;

                  //ここで$result[0]['○○']を使わないのは、$resultの値がsqlから引っ張られているものだから…であってる？

    try
    {
      $sql = $pdo -> prepare($query_str);
      $sql -> execute();
    }
    catch (PDOException $e)
    {
      print $e -> getMessage();
    }

    //echo $query_str;

    $url = "detail01.php?id=" . $param_id;
    header('Location: ' . $url);
    exit;

  ?>
