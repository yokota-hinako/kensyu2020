<!DOCTYPE html>
  <?php
    // パラメーターチェック
    $param_name = "";
    if (isset($_GET['namae']) && $_GET['namae'] !="")
    {
     $param_name = $_GET['namae'];
    }

    $param_sex = "";
    if (isset($_GET['sex']) && $_GET['sex'] !="")
    {
     $param_sex = $_GET['sex'];
    }

    $param_section = "";
    if (isset($_GET['section']) && $_GET['section'] !="")
    {
     $param_section = $_GET['section'];
    }

    $param_grade = "";
    if (isset($_GET['grade']) && $_GET['grade'] !="")
    {
     $param_grade = $_GET['grade'];
    }

    // DB接続処理+部署役職の呼び出し
    include("./include/functions.php");
    $pdo = commonDB();
    $query_str = "SELECT
                    m.member_ID,
                    m.name,
                    m.pref,
                    m.seibetu,
                    sm.section_name,
                    gm.grade_name
                  FROM member AS m
                  LEFT JOIN section1_master AS sm ON sm.ID = m.section_ID
                  LEFT JOIN grade_master AS gm ON gm.ID = m.grade_ID
                  WHERE 1=1 "; //「WHERE 1=1」は全件表示の意味＋後ろに検索条件が来た場合に、ANDでくっつけてくれる役割がある

                  if($param_name != "")
                  // /if (isset($_GET['namae']) && $_GET['namae'] !="")でも良いが、3行目ですでに「$param_name」と定義した後なのでわざわざ書く必要性はない
                  {
                    $query_str .= " AND m.name LIKE '%" . $_GET['namae'] . "%' ";
                  }//「○○ .= △△」はイコールの前の値（○○）に後ろの値（△△）をくっつける、という意味の「結合演算子」

                  if($param_sex != "")
                  {
                    $query_str .= " AND m.seibetu = " . $_GET['sex'];
                  }

                  if($param_section != "")
                  {
                    $query_str .= " AND m.section_ID = " . $_GET['section'];
                  }// ここのWHEREは「member」のなかの値から引っ張ってこないといけないため、「sm.section_name」ではエラーになる

                  if($param_grade != "")
                  {
                    $query_str .= " AND m.grade_ID = " . $_GET['grade'];
                  }

                 //ここのWHEREも「member」のなかの値から引っ張ってこないといけないため、「gm.grade_name」ではエラーになる

                // 以下間違いコード例
                 //   WHERE m.name LIKE '%" . $_GET[name] . "%'
                 //   AND m.seibetu =  " . $_GET['sex'] . "
                 //   AND sm.section_ID =  " . $_GET['section'] . "
                //   AND gm.grade_ID =  " . $_GET['grade'] . "
                 // ";

    $sql = $pdo->prepare($query_str);
    $sql->execute();
    $result = $sql->fetchAll();
  ?>

  <html>
    <head>
      <meta charset=utf-8>
      <meta name="viewport" content="width=device-width,initial-scale=1">
      <link rel="stylsheet" href="./include/style.css">

      <!-- bootstrap用 -->
      <?php include("./include/bootstrap.php"); ?>
      <style type="text/css">
        #resulttable
        {
          text-align: center;
        }

        #resultTdiv
        {
          text-align: center;
        }

        #resultth
        {
          background-color: aliceblue;
        }

        table tr:hover
        {
          background-color: floralwhite;
        }
     </style>

      <!-- 入力内容のクリア -->
      <script type="text/javascript">
        function clearForm()
        {
          document.serchform.namae.value="";//←これがDOM操作
          document.serchform.sex.value="";
          document.serchform.section.value="";
          document.serchform.grade.value="";
        }
      </script>

      <!-- タイトル -->
      <title>社員一覧</title>
    </head>

    <body>
      <!-- ヘッダー -->
      <?php
        include("./include/header.php");
      ?>

      <!-- 入力部 -->
      <form  name="serchform" action="./index.php" method="get">
        <div id=resultTdiv>
          <label for="namae">名前：</label>
          <input
            type="text"
            name="namae"
            max="30"
            placeholder="社員名を入力して下さい"
            value="<?php echo $param_name; ?>"
          >

        <div id=resultTdiv>
          <label for="sex">性別：</label>
            <select name="sex">
              <option value="" <?php if ($param_sex == "") { echo "selected" ; } ?>>すべて</option>
              <option value="0" <?php if ($param_sex == "0") { echo "selected" ; } ?>>女</option>
              <option value="1" <?php if ($param_sex == "1") { echo "selected" ; } ?>>男</option>
              <option value="2" <?php if ($param_sex == "2") { echo "selected" ; } ?>>その他</option>
            </select>

          <label for="section">部署:</label>
            <select name="section">
              <option value="">すべて</option>
              <?php
                $result_section = commonSection();
                foreach ($result_section as $each)
                {
                  if ($each['ID'] == $param_section)
                  {
                    echo "<option value='" . $each['ID'] . "' selected>" . $each['section_name'] . "</option>";
                  }
                  else
                  {
                    echo "<option value='" . $each['ID'] . "'>" . $each['section_name'] . "</option>";
                  }
                }
              ?>
            </select>

          <label for="grade">役職:</label>
            <select name="grade">
              <option value="">すべて</option>
              <?php
                $result_grade = commonGrade();
                foreach ($result_grade as $each)
                {
                  if ($each['ID'] == $param_grade)
                  {
                    echo "<option value='" . $each['ID'] . "' selected>" . $each['grade_name'] . "</option>";
                  }
                  else
                  {
                    echo "<option value='" . $each['ID'] . "'>" . $each['grade_name'] . "</option>";
                  }
                }
              ?>
            </select>
        </div>

        <div id=resultbutton>
          <input type="submit" value="  検索  " class="btn btn-primary btn-sm">
          <input type="button" value="リセット" onclick="clearForm();" class="btn btn-outline-primary btn-sm">
        </div>
      </form>
      <hr/>

      <!-- 出力部 -->
      検索結果：
      <?php echo count($result) ?>

      <!-- <table border="1" style="border-collapse:collapse;"> -->
      <table class='table table-bordered' id='resulttable'>
        <thead>
          <tr id='resulttr'>
            <th id=resultth>社員ID</th>
            <th id=resultth>名前</th>
            <th id=resultth>部署</th>
            <th id=resultth>役職</th>

            <?php
              if (count($result) == 0)
              {
                echo "<tr><td colspan='4'>" . "検索結果なし" . "</td></tr>";
              }
              else
              {
                foreach ($result as $each)
                  {
                    echo
                      "<tr>"
                        . "<td>" . $each['member_ID'] . "</td>"
                        . "<td><a href='./detail01.php?id=" . $each['member_ID'] . "'>" . $each['name'] . "</a></td>"
                        . "<td>" . $each['section_name'] . "</td>"
                        . "<td>" . $each['grade_name'] . "</td>"
                      . "</tr>" ;
                  }
              }
            ?>

            <!-- 生データ取得 -->
            <!-- <pre>
            <php var_dump($result); ?>
            </pre> -->

          </tr>
        </thead>
      </table>
      <br>
      <hr/>
    </body>
  </html>
