<!DOCTYPE html>
<html>
  <head>
    <meta charset=utf-8>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>第3回：分岐と繰り返し02（結果画面）</title>
  </head>
  <body>
    <?php
      $gold_pw = "gold001";
      $gold_id = "kizoku";

      $reg_pw = "blue001";
      $reg_id = "heimin";

      if($_POST['id1'] == $gold_pw AND $_POST['pw1'] == $gold_id)
      {
        echo "ようこそいらっしゃいました、ゴールド会員さま";
      }
      elseif($_POST['id1'] == $reg_pw AND $_POST['pw1'] == $reg_id)
      {
        echo "こんにちは、ノーマル会員さん";
      }
      else
      {
        echo "ログインID、またはログインパスワードが間違っています";
      }
     ?><br>
    <br>
    <form action="./login02.php" method="post">
      <input type="submit" value="ログイン画面に戻る">
    </form>
  </body>
</html>
