<!DOCTYPE html>
  <?php
    include("./include/statics.php");// 目的の都道府県ファイル呼び出し（）内はパスと呼ばれるもの
    include("./include/functions.php");
    $result_section = commonSection();//DB接続部署呼び出し
    $result_grade = commonGrade();//DB接続役職呼び出し
   ?>

  <!-- 生データ取得 -->
  <!-- <pre>
    <php var_dump($result); ?>
  </pre> -->

  <html>
    <head>
      <meta charset=utf-8>
      <meta name='viewport' content='width=device-width,initial-scale=1'>
      <link rel='stylsheet' href='./include/style.css'>

      <!-- bootstrap用 -->
      <?php include("./include/bootstrap.php"); ?>
      <style type="text/css">
        #resultth
        {
          background-color: antiquewhite;
          text-align: center;
          width: 200px;
        }
     </style>

      <title>新規社員情報登録</title>
      <script src = "./include/functions.js"></script><!-- Javascriptで入力チェック&モーダル表示 -->
    </head>
    <body>

      <!-- ヘッダー -->
      <?php
        include("./include/header.php");
      ?>

      <!-- 表示部 -->
      <form  name="entry" action="./entry02.php" method="POST">
        <table class = 'table table-bordered' id='resulttable'>
          <tr>
            <th id=resultth>名前</th>
              <td>
                  <input
                    type="text"
                    name="new_namae"
                    max="30"
                    placeholder="全角ひらがな・カタカナ"
                  >
              </td>
          </tr>
          <tr>
            <th id=resultth>出身地</th>
              <td>
                <select name="new_pref">
                  <option value="" style="display: none;">都道府県</option>
                    <?php
                      foreach ($pref_array as $key => $value)
                      {
                        echo "<option value='" . $key . "'>" . $value . "</option>";
                      }
                    ?>
                     <!-- 3行目の「include」ですでにファイル内にstatics.phpを取り込んでいるため、ここの$pref_arrayで値を都道府県の値を取ってこれているよ～ -->
                </select>
              </td>
          </tr>
          <tr>
            <th id=resultth>性別</th>
              <td>
                <label><input type="radio" name="new_sex" value="1" checked="checked">男</label>
                <label><input type="radio" name="new_sex" value="0">女</label>
                <label><input type="radio" name="new_sex" value="2">その他</label>
              </td>
          </tr>
          <tr>
            <th id=resultth>年齢</th>
              <td>
                <input
                  type="number"
                  min="0"
                  name="new_age"
                  placeholder="半角英数"
                >
                才
              </td>
          </tr>
          <tr>
            <th id=resultth>所属部署</th>
              <td>
                <?php
                  foreach ($result_section as $each)
                  {
                    if ($each['ID'] == 1)
                    {
                      echo "<label><input type='radio' name='new_section' value='" . $each['ID'] . "' checked>" . $each['section_name'] . "</label> &nbsp";
                    }
                    else
                    {
                      echo "<label><input type='radio' name='new_section' value='" . $each['ID'] . "'>" . $each['section_name'] . "</label> &nbsp";
                    }
                  }
                ?>
              </td>
          </tr>
          <tr>
            <th id=resultth>役職</th>
              <td>
                <?php
                  foreach ($result_grade as $each)
                  {
                    if ($each['ID'] == 1)
                    {
                      echo "<label><input type='radio' name='new_grade' value='" . $each['ID'] . "' checked>" . $each['grade_name'] . "</label> &nbsp";
                    }
                    else
                    {
                      echo "<label><input type='radio' name='new_grade' value='" . $each['ID'] . "'>" . $each['grade_name'] . "</label> &nbsp";
                    }
                  }
                ?>
              </td>
          </tr>
        </table>
        <br>
        <!-- 登録・リセットボタン -->
        <div id=resultbutton>
          <input type='hidden' value='<?php echo $result[0]['member_ID']; ?>' name='member_ID'>
          <input type="button" value=" 登録 " onclick="goNew();" class="btn btn-primary">
          <input type="reset" value="リセット" class="btn btn-outline-primary">
        </div>
      </form>
    </body>
  </html>
