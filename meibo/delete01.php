<!DOCTYPE html>
  <!-- 生データ取得 -->
  <pre>
  <?php  var_dump($_POST); ?>
  </pre>

  <?php
    include("./include/functions.php");//エラー処理の設定

    // パラメーターチェック
    //主キーだけ消せば他情報も消えるため、member_IDだけで良し
    $member_id = "";
    if (isset($_POST['member_ID']) AND $_POST['member_ID'] !="")
    {
      $member_id = $_POST['member_ID'];
    }
    else
    {
      commonError();
    }

    $pdo = commonDB();

    // 削除を実行するための命令文(エイリアスは不要)
    $query_str = "DELETE FROM member
                  WHERE member_ID = " . $member_id;


    try
    {
      $sql = $pdo -> prepare($query_str);
      $sql -> execute();
    }
    catch (PDOException $e)
    {
      print $e -> getMessage();
    }

    echo $query_str;

    $url = "./index.php";//処理が終わったらここに遷移しろよ～という命令文
    header('Location: ' . $url);
    exit;

  ?>
