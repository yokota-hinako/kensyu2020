<!DOCTYPE html>
<html>
  <head>
    <meta charset=utf-8>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>第5回：連想配列、配列のネスト</title>
  </head>
  <body>
    <h1>二次元配列</h1>
    <?php
      $team_a = array("あ","い","う","え","お");
      $team_b = array("か","き","く","け","こ");
      $team_c = array("さ","し","す","せ","そ");
      $team_d = array("た","ち","つ","て","と");
      $team_e = array("な","に","ぬ","ね","の");

      $team_all = array($team_a, $team_b, $team_c, $team_d, $team_e);

      echo "<pre>";
      var_dump($team_all);
      echo "</pre>";

      echo "<table border='1'>";
      foreach($team_all as $team)//まず「$team_all」のなかの「$team」を引っ張り出す
      {
        echo "<tr>";
        foreach($team as $value)//さらに「$team」のなかの「$value」を持ってくるよう指定する
        {
          echo "<td>" . $value . "</td>";
        }
        echo "</tr>";
      }
      echo "</table>";

    ?>
  </body>
</html>
