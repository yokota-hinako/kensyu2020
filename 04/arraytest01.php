<!DOCTYPE html>
<html>
  <head>
    <meta charset=utf-8>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>第4回：配列</title>
  </head>
  <body>
    <?php
      $sweet = array("プリン", "アイス", "シュークリーム", "チーズケーキ", "イチゴショート", "マカロン","メレンゲ","");
        echo $sweet[3];
        echo $sweet[0];
        echo $sweet[9];
      $sweet[2] = "ジェラート";
      $sweet[6] = "チョコレート";

      var_dump($sweet);

      echo "<hr>";//<hr>⇒線引っ張ってくれる

      echo "<table border='1'>";
      for($i = 0; $i < count($sweet); $i++)//「++」は「+1」していくという意味で、「$i = $1+1」とかでも代用可能
        {
        echo "<tr><td>" . $sweet[$i] . "</td></tr>";//$sweet[$i]⇒$seeetの値を[$i]の処理だけ表示させるという意味だよ～
        }
      echo "</table>";

      echo "<hr>";

      echo "<table border='1'>";
      foreach($sweet as $each)//「foreach」は配列の中身を、入っている数だけ処理してくれる
      {
        echo "<tr><td>" . $each .  "</td></tr>";
      }
      echo "</table>";

      echo "<hr>";

      foreach($sweet as $key => $value)//大なりイコールの記号そのものにはあまり意味がない
      {
        echo $key . "番目の要素は" . $value . "です。<br/>";
      }

      echo "<hr>";

      $needle = "アイス";//検索したい文字列
      if(in_array($needle, $sweet))//←第１引数「$needle」と同じ値を、第2引数「$sweet」の配列から探してね～という命令文
      {
        echo "おめでとうございます！「" . $needle . "」がsweetの要素の値に見つかりました。";
      }
      else
      {
        echo "すみません、「" . $needle . "」がsweetの要素の値に見つかりませんでした。<br/>";
      }

      echo "<hr>";

     ?>

    配列の個数は<?php echo count($sweet); ?>
     <pre>
       <?php
          var_dump($sweet);
        ?>
     </pre>
  </body>
</html>
