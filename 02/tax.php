<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>第二回課題、フォーム部品練習</title>
  </head>
  <body>
    <form action="tax.php" method="post">
      <table border="1" style="border-collapse:collapse;">
        <tr>
          <th>商品名</th>
          <th>価格（単位：円、税抜き）</th>
          <th>個数</th>
          <th>税率</th>
        </tr>
        <tr>
          <td>1.<input type="text" name="shina1"></td>
          <td><input type="number" name="kakaku1">円</td>
          <td><input type="number" name="kosu1">個</td>
          <td><input type="radio" name="zei1" value=8.0%>8%
              <input type="radio" name="zei1" value=10.0%>10%</td>
        </tr>
        <tr>
          <td>2.<input type="text" name="shina2"></td>
          <td><input type="number" name="kakaku2">円</td>
          <td><input type="number" name="kosu2">個</td>
          <td><input type="radio" name="zei2" value=8.0%>8%
              <input type="radio" name="zei2" value=10.0%>10%</td>
        </tr>
        <tr>
          <td>3.<input type="text" name="shina3"></td>
          <td><input type="number" name="kakaku3">円</td>
          <td><input type="number" name="kosu3">個</td>
          <td><input type="radio" name="zei3" value=8.0%>8%
              <input type="radio" name="zei3" value=10.0%>10%</td>
        </tr>
        <tr>
          <td>4.<input type="text" name="shina4"></td>
          <td><input type="number" name="kakaku4">円</td>
          <td><input type="number" name="kosu4">個</td>
          <td><input type="radio" name="zei4" value=8.0%>8%
              <input type="radio" name="zei4" value=10.0%>10%</td>
        </tr>
        <tr>
          <td>5.<input type="text" name="shina5"></td>
          <td><input type="number" name="kakaku5">円</td>
          <td><input type="number" name="kosu5">個</td>
          <td><input type="radio" name="zei5" value=8.0%>8%
              <input type="radio" name="zei5" value=10.0%>10%</td>
        </tr>
      </table>
      <input type="submit" value="計算する">
      <input type="reset" value="リセット">
    </form><br>
    <br>

    <form action="tax.php" method="post">
      <table border="1" style="border-collapse:collapse;">
        <tr>
          <th>商品名</th>
          <th>価格（単位：円、税抜き）</th>
          <th>個数</th>
          <th>税率</th>
          <th>小計（単位：円）</th>
        </tr>
        <tr>
          <td>1.
            <?php
              echo $_POST['shina1'];
             ?>
          </td>
          <td>
            <?php
              echo $_POST['kakaku1'];
             ?>
          </td>
          <td>
            <?php
              echo $_POST['kosu1'];
             ?>
          </td>
          <td>
            <?php
              echo $_POST['zei1'];
             ?>
          </td>
          <td>
            <?php
              $price = $_POST['zei1'] / 100 + 1;
              $subtotal1 = $_POST['kakaku1'] * $_POST['kosu1'] * $price;
              echo $subtotal1 . "円（税込み）";
             ?>
          </td>
        </tr>
        <tr>
          <td>2.
            <?php
              echo $_POST['shina2'];
             ?>
          </td>
          <td>
            <?php
              echo $_POST['kakaku2'];
             ?>
          </td>
          <td>
            <?php
              echo $_POST['kosu2'];
             ?>
          </td>
          <td>
            <?php
              echo $_POST['zei2'];
             ?>
          </td>
          <td>
            <?php
              $price = $_POST['zei2'] / 100 + 1;
              $subtotal2 = $_POST['kakaku2'] * $_POST['kosu2'] * $price;
              echo $subtotal2 . "円（税込み）";
             ?>
          </td>
        </tr>
        <tr>
          <td>3.
            <?php
              echo $_POST['shina3'];
             ?>
          </td>
          <td>
            <?php
              echo $_POST['kakaku3'];
             ?>
          </td>
          <td>
            <?php
              echo $_POST['kosu3'];
             ?>
          </td>
          <td>
            <?php
              echo $_POST['zei3'];
             ?>
          </td>
          <td>
            <?php
              $price = $_POST['zei3'] / 100 + 1;
              $subtotal3 = $_POST['kakaku3'] * $_POST['kosu3'] * $price;
              echo $subtotal3 . "円（税込み）";
             ?>
          </td>
        </tr>
        <tr>
          <td>4.
            <?php
              echo $_POST['shina4'];
             ?>
          </td>
          <td>
            <?php
              echo $_POST['kakaku4'];
             ?>
          </td>
          <td>
            <?php
              echo $_POST['kosu4'];
             ?>
          </td>
          <td>
            <?php
              echo $_POST['zei4'];
             ?>
          </td>
          <td>
            <?php
              $price = $_POST['zei4'] / 100 + 1;
              $subtotal4 = $_POST['kakaku4'] * $_POST['kosu4'] * $price;
              echo $subtotal4 . "円（税込み）";
             ?>
          </td>
        </tr>
        <tr>
          <td>5.
            <?php
              echo $_POST['shina5'];
             ?>
          </td>
          <td>
            <?php
              echo $_POST['kakaku5'];
             ?>
          </td>
          <td>
            <?php
              echo $_POST['kosu5'];
             ?>
          </td>
          <td>
            <?php
              echo $_POST['zei5'];
             ?>
          </td>
          <td>
            <?php
              $price = $_POST['zei5'] / 100 + 1;
              $subtotal5 = $_POST['kakaku5'] * $_POST['kosu5'] * $price;
              echo $subtotal5 . "円（税込み）";
             ?>
          </td>
        </tr>
        <tr>
          <td colspan="4"><b>合計</b></td>
          <td>
            <?php
              $totalprice = $subtotal1 + $subtotal2 + $subtotal3 + $subtotal4 + $subtotal5;
              echo $totalprice . "円（税込み）";
             ?>
          </td>
        </tr>


      </table>
    </form>

  </body>

</html>
