<!DOCTYPE html>
<html>
  <head>
    <meta charset=utf-8>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>第3回：分岐と繰り返し03</title>
  </head>
  <body>
    <h1>座標テーブル</h1>
    <h3>入力</h3>
    <form action="./loop03.php" method="get">
      <input type="number" name="form1"  placeholder="半角数字">行
      ×
      <input type="number" name="form2"  placeholder="半角数字">列
      <br>
      <input type="submit" value="送信">
      <input type="reset" value="リセット">

    </form>
    <br>
    <h3>作成結果</h3>
    <table border="1" style="border-collapse:collapse;">
        <?php
          for($i =1; $i <= $_GET['form2']; $i++)
          {
            echo "<tr>";
            for($j =1; $j <= $_GET['form1']; $j++)
            {
            echo
              "<td> $i - $j </td>";
            }
          echo "</tr>";
          }
         ?>
    </table>
  </body>
</html>
