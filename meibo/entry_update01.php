<!DOCTYPE html>
  <html>
    <head>
      <meta charset=utf-8>
      <meta name="viewport" content="width=device-width,initial-scale=1">

      <script type="text/javascript">//登録モーダル
        function goUpdate()
        {
          if(window.confirm('更新を行います。よろしいですか？'))
          {
            document.update.submit();//trueの際に行われる処理
          }
        }
      </script>

      <!-- bootstrap用 -->
      <?php include("./include/bootstrap.php"); ?>
      <style type="text/css">
        #resultth
        {
          background-color: antiquewhite;
          text-align: center;
          width: 200px;
        }
     </style>

      <title>社員詳細情報編集</title>
      <!-- ここ「社員情報詳細編集」じゃないの気持ち悪いな～ -->
    </head>
    <body>

      <!-- ヘッダー -->
      <?php
        include("./include/header.php");//ヘッダーファイルの指定
      ?>

      <!-- 前処理 -->
      <?php
        include("./include/statics.php");//性別・出身地ファイルの指定
        include("./include/functions.php");//エラー・DBファイルの指定

        $param_id = "";
        if (isset($_GET['id']) && $_GET['id'] !="" && is_numeric($_GET['id']))// IDのパラメーターチェックと、その値が数字であるかのチェック
        {
          $param_id = $_GET['id'];//データ内にidあった場合の処理
        }
        else
        {
          commonError();//チェックが通らなかった場合の処理
        }

        $pdo = commonDB();//改めてDB呼び出し

        $query_str = "SELECT
                        m.member_ID,
                        m.name,
                        m.pref,
                        m.seibetu,
                        m.age,
                        m.section_ID,
                        m.grade_ID
                      FROM member AS m
                      WHERE m.member_ID = " . $param_id;

        $sql = $pdo->prepare($query_str);
        $sql->execute();
        $result = $sql->fetchAll();
        if (count($result) != 1)
        {
          commonError();
        }
      ?>

      <!-- 生データ取得 -->
      <!-- <pre>
       <php
          var_dump($result);
       ?>
      </pre> -->

      <!-- 表示部  -->
      <form name="update" action='./entry_update02.php' method='POST'>
        <table class='table table-bordered' id='resulttable' style="border-collapse:collapse;">
          <tr>
            <th id=resultth>社員ID</th>
            <td>
              <?php echo $result[0]['member_ID'];?>
            </td>
          <tr>
            <th id=resultth>名前</th>
            <td>
              <input
                type="text"
                max="30"
                name="up_namae"
                value= <?php echo $result[0]['name'];?>
              >
            </td>
          </tr>
          <tr>
            <th id=resultth>出身地</th>
            <td>
              <select name="up_pref">
                <option value="" style="display: none;">都道府県</option>
                  <?php
                    foreach ($pref_array as $key => $value)
                    {
                      if ($result[0]['pref'] == $key)//$result[0]['pref']の値が$keyと同じなら…
                      {
                        echo "<option value='" . $key . "'selected>" . $value . "</option>";//その$key番目の$valueをselected（表示）してね～
                      }
                      else
                      {
                        echo "<option value='" . $key . "'>" . $value . "</option>";//違ったら回し続けて探してよ～
                      }
                    }
                   ?>
              </select>
            </td>
          </tr>
          <tr>
            <th id=resultth>性別</th>
              <td>
                <label>
                  <input
                    type="radio"
                    name="up_sex"
                    value='1'
                    <?php if ($result[0]['seibetu'] == '1') {echo "checked";} ?>
                  >男
                </label>

                <label>
                  <input
                    type="radio"
                    name="up_sex"
                    value='0'
                    <?php if ($result[0]['seibetu'] == '0') {echo "checked";} ?>
                  >女
                </label>

                <label>
                  <input
                    type="radio"
                    name="up_sex"
                    value='2'
                    <?php if ($result[0]['seibetu'] == '2') {echo "checked";} ?>
                  >その他
                </label>
              </td>
          </tr>
          <tr>
            <th id=resultth>年齢</th>
              <td>
                  <input
                    type="number"
                    min="0"
                    name="up_age"
                    value='<?php echo $result[0]['age'];?>'
                  >
                  才
              </td>
          </tr>
          <tr>
            <th id=resultth>所属部署</th>
              <td>
                <?php
                  $result_section = commonSection();
                  foreach ($result_section as $each)
                  {
                    if ($each['ID'] == $result[0]['section_ID'])
                    {
                      echo "<label><input type='radio' name='up_section' value='" . $each['ID'] . "' checked>" . $each['section_name'] . "</label> &nbsp";
                    }
                    else
                    {
                      echo "<label><input type='radio' name='up_section' value='" . $each['ID'] . "'>" . $each['section_name'] . "</label> &nbsp";
                    }
                  }
                ?>
              </td>
          </tr>
          <tr>
            <th id=resultth>役職</th>
              <td>
                <?php
                  $result_grade = commonGrade();
                  foreach ($result_grade as $each)
                  {
                    if ($each['ID'] == $result[0]['grade_ID'])
                    {
                      echo "<label><input type='radio' name='up_grade' value='" . $each['ID'] . "' checked>" . $each['grade_name'] . "</label> &nbsp";
                    }
                    else
                    {
                      echo "<label><input type='radio' name='up_grade' value='" . $each['ID'] . "'>" . $each['grade_name'] . "</label> &nbsp";
                    }
                  }
                ?>
              </td>
          </tr>
        </table>
        <br>
        <!-- 登録・リセットボタン -->
        <div id=resultbutton>
          <input type='hidden' value='<?php echo $result[0]['member_ID']; ?>' name='member_ID'>
          <input type='button' value=' 登録 ' onclick='goUpdate();' class="btn btn-primary">
          <input type="reset" value="リセット" class="btn btn-outline-primary">
        </div>
      </form>
    </body>
  </html>
