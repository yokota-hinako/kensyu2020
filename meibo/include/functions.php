<!-- エラーが出たらここに飛ばしてまとめて処理しよ～という発想 -->
<!-- ついでにDBの呼び出しもここでまとめて共通化しちゃう -->
<?php
//エラー処理
  function commonError()
  {
    echo "エラーが発生しました。<br/>";
    echo "<a href = './index.php'>トップページに戻る</a>";
    echo "</body>";
    echo "</html>";
    exit();
  }

//DB接続
  function commonDB()
  {
    $DB_DSN = "mysql:host=localhost; dbname=hyokota; charset=utf8";
    $DB_USER = "webaccess";
    $DB_PW = "toMeu4rH";
    $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);//引数
    return $pdo;//返り値（$pdoの結果をreturnで返している）
  }

//担当部署
  function commonSection()
  {
    $pdo = commonDB();
    $query_str = "SELECT *
                  FROM section1_master";
    $sql = $pdo->prepare($query_str);
    $sql->execute();
    $result_section = $sql->fetchAll();
    return  $result_section;
  }

//役職
  function commonGrade()
  {
    $pdo = commonDB();
    $query_str2 = "SELECT *
                  FROM grade_master";
    $sql2 = $pdo->prepare($query_str2);
    $sql2->execute();
    $result_grade = $sql2->fetchAll();
    return  $result_grade;
  }
 ?>
