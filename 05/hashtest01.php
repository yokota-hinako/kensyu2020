<!DOCTYPE html>
<html>
  <head>
    <meta charset=utf-8>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>第5回：連想配列、配列のネスト</title>
  </head>
  <body>
    <h1>ハッシュテーブル</h1>
    <?php
        $yokota_data = array
            (
              'sweet' => 'シュークリーム',
              'game' => 'ポケモン',
              'town' => '郡山',
              'age' => 12,
              'food' => '納豆ご飯'
            );

        echo $yokota_data['town']; // 横浜 と表示される

        echo $yokota_data['age']; // 21 と表示される

        echo $yokota_data['school']; // エラーになる

        $yokota_data['age'] = 23; // 上書きされる

        var_dump($yokota_data); // 配列の中身がすべて表示される

        echo "<hr>";

        echo "<table border='1'>";
        foreach($yokota_data as $each)
        {
          echo "<tr><td>" . $each . "</td></tr>";
        }

          foreach($yokota_data as $key => $value)
          {
            echo "<tr><td>" . $key . " : " . $value . "</td></tr>";
          }
        echo "</table>";

    ?>

    <pre>
    <?php var_dump($yokota_data); ?>
    </pre>

  </body>
</html>
