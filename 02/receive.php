<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content="width=device-width, initial-scale=1">
    <title>第2回 フォーム部品練習(反映画面)</title>
  </head>
  <body>
      こんにちは、
      <?php
        echo $_GET['name1'];
       ?>
       さん。こちらは入力内容の確認画面です！<br>
       <br>
      今日の天気：
        <?php
          echo $_GET['example'];
         ?><br>
      いまの気分：
        <?php
          if(isset($_GET['mind1']))
          {
            $checkbox = $_GET['mind1'];
            echo $checkbox . '<br>';
          }
          else
          {
            echo 'チェックボックスを選択してください。<br>';
          }
         ?>
      選択したプルダウン：
        <?php
          if(isset($_GET['puru']))
          {
            $puru = $_GET['puru'];
            echo $puru . '<br>';
          }
          else
          {
            echo 'プルダウンを選択してください。<br>';
          }
         ?>
      自由入力欄：
        <?php
          if(isset($_GET['comment']))
          {
            $comment = $_GET['comment'];
            echo $comment . '<br>';
          }
          else
          {
            echo '内容を入力してください。<br>';
          }
         ?><br>
  </body>
</html>
