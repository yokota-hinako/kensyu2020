<!DOCTYPE html>
<html>
  <head>
    <meta charset=utf-8>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>第5回：連想配列、配列のネスト</title>
  </head>
  <body>
    <h1>連想配列の組み合わせ</h1>
    <?php
      $pokemon = array
      (
        array
        (
          "No." => "816",
          "name" => "メッソン",
          "type1" => "みず",
          "type2" => "-",
        ),
        array
        (
          "No." => "817",
          "name" => "ジメレオン",
          "type1" => "みず",
          "type2" => "-",
        ),
        array
        (
          "No." => "818",
          "name" => "インテレオン",
          "type1" => "みず",
          "type2" => "-",
        ),
        array
        (
          "No." => "728",
          "name" => "アシマリ",
          "type1" => "みず",
          "type2" => "-",
        ),
        array
        (
          "No." => "729",
          "name" => "オシャマリ",
          "type1" => "みず",
          "type2" => "-",
        ),
        array
        (
          "No." => "730",
          "name" => "アシレーヌ",
          "type1" => "みず",
          "type2" => "フェアリー",
        )
      );

      echo "<pre>";
      var_dump($pokemon);
      echo "</pre>";

      // echo "<table border='1'>";
      // echo "<tr>";
      foreach($pokemon as $each)
      {
        echo
          "No.: " . $each['No.'] . ", "
          . "名前: " . $each['name'] . ", "
          . "タイプ1: " . $each['type1'] . ", "
          . "タイプ2: " . $each['type2'] . "<br>";
      }
      // echo "</tr>";
      // echo "</table>";
      echo "<hr>";
      ?>

    <table border='1'>
      <tr>
        <th>No.</th>
        <th>名前</th>
        <th>タイプ1</th>
        <th>タイプ2</th>
      <?php
        foreach($pokemon as $each)
          {
            echo
              "<tr>"  //78と94で<tr></tr>で囲う＋ここでさらに囲うことで、横一列に長い表が改善される
              . "<td>" . $each['No.'] . "</td>"
              . "<td>" . $each['name'] . "</td>"
              . "<td>" . $each['type1'] . "</td>"
              . "<td>" . $each['type2'] . "</td>"
              . "<tr>";
          }
       ?>
      </tr>
    </table>
  </body>
</html>
