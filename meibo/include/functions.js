//入力チェック＆モーダル表示
  function goNew()
  {
    var temp_name = document.entry.new_namae.value;
    temp_name = temp_name.replace(/\s+/g,"");//名前の中のスペースを置き換える
    document.entry.new_namae.value = temp_name;
    if(temp_name == "")
    {
      window.alert('名前は必須です');//trueの際に行われる処理
      return false;//「false」を返して更新処理にいかないようにする
    }

    if(document.entry.new_pref.value == "")
    {
      window.alert('都道府県は必須です');
      return false;
    }

    var temp_age = document.entry.new_age.value;
    if(temp_age == "")
    {
      window.alert('年齢は必須です');//年齢の値が入っているかのチェック
      return false;
    }
    var regex = new RegExp(/^[0-9]+$/);
    if(temp_age < 1 || temp_age > 99 || !regex.test(temp_age))//入力値の大きさと小数点の規制
    {
      window.alert('年齢は1-99の範囲で入力してください');//エラー原因が異なる場合は別で表示したいため、仕様書と異なる仕様で制作
      return false;
    }


    if(window.confirm('更新を行います。よろしいですか？'))
    {
      document.entry.submit();//trueの際に行われる処理
    }
  }

  //名前の入力チェック
