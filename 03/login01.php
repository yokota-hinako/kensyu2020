<!DOCTYPE html>
<html>
  <head>
    <meta charset=utf-8>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>第3回：分岐と繰り返し（ログイン画面）</title>
  </head>
  <body>
    <form type="text" action="result01.php " method="post">
      <label for="id1">ID:</label><br>
        <input type="text" name="id1" maxlength="10"><br>
        <br>
      <label for="pw1">パスワード:</label><br>
        <input type="password" name="pw1" maxlength="10"><br>
        <br>
        <input type="submit" value="ログイン">
        <input type="reset" value="リセット">
    </form>
  </body>
</html>
