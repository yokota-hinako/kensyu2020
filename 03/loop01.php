<!DOCTYPE html>
<html>
  <head>
    <meta charset=utf-8>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>第3回：分岐と繰り返し03</title>
  </head>
  <body>
    <h3>入力</h3>
    <form action="loop01.php" method="get">
      <input type="number" name="form"  placeholder="半角数字">
        <h7>行のテーブルを作る</h7><br>
      <br>
      <input type="submit" value="送信">
      <input type="reset" value="リセット">

    </form>
    <br>
    <h3>作成結果</h3>
    <form action="loop01.php" method="get">
      <table border="1" style="border-collapse:collapse;">
        <?php
          for($i=0; $i < $_GET['form']; $i++)
          {
            echo
              "<tr>
                <td>ずん♪</td>
                <td>ずん♪</td>
                <td>ずん♪</td>
                <td>ずんどこ♪</td>
                <td>キヨシ♪</td>
              </tr>";
          }
         ?>
      </table>
    </form>
  </body>
</html>
