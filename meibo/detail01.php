<!DOCTYPE html>
  <html>
  <!-- 属性などは「''」でくくるとインデントがきれい -->
    <head>
      <meta charset=utf-8>
      <meta name="viewport" content="width=device-width,initial-scale=1">
      <link rel="stylsheet" href="./include/style.css">

      <!-- 削除モーダル -->
      <script type="text/javascript">
        function goDel()
        {
          if(window.confirm('削除を行います。よろしいですか？'))
          {
            document.delete.submit();//trueの際に行われる処理
          }
        }
      </script>

      <!-- bootstrap用 -->
      <?php include("./include/bootstrap.php"); ?>
      <style type="text/css">
        #resultth
        {
          background-color: aliceblue;
          width: 200px;
          text-align: center;
        }

        #resultbutton
        {
          margin: 0px 550px;
          display: inline-flex;
        }
     </style>

      <!-- タイトル -->
      <title>社員情報詳細</title>
    </head>
    <body>

      <!-- ヘッダー -->
      <?php
        include("./include/header.php");
      ?>

      <?php
        include("./include/statics.php");//性別・出身地ファイルの指定
        include("./include/functions.php");//エラー・DBファイルの指定

        $param_id = "";
        if (isset($_GET['id']) && $_GET['id'] !="" && is_numeric($_GET['id']))// IDのパラメーターチェックと、その値が数字であるかのチェック
        {
         $param_id = $_GET['id'];//データ内にidあった場合の処理
        }
        else
        {
          commonError();//チェックが通らなかった場合の処理
        }

        $pdo = commonDB();//改めてDB呼び出し

        $query_str = "SELECT
                        m.member_ID,
                        m.name,
                        m.pref,
                        m.seibetu,
                        m.age,
                        sm.section_name,
                        gm.grade_name
                      FROM member AS m
                      LEFT JOIN section1_master AS sm ON sm.ID = m.section_ID
                      LEFT JOIN grade_master AS gm ON gm.ID = m.grade_ID
                      WHERE m.member_ID = " . $param_id;

        $sql = $pdo->prepare($query_str);
        $sql->execute();

        // 55~59行：検索結果が「1つ」以上あったときの処理
        $result = $sql->fetchAll();
        if (count($result) != 1)
        {
          commonError();
        }
      ?>

      <!-- 表示部 -->
      <table class='table table-bordered' id='resulttable'>
        <tr>
          <th id=resultth>社員ID</th>
          <td>
            <?php echo $result[0]['member_ID'];?>
          </td>
          <!-- ↑は$result[0]という箱の中の['member_ID']を呼び出す、という意味のコードである
              ['member_ID']は$result[0]のなかの配列0番目の値なので、 $result[0][0]でも同じ出力になる
              詳しくは『2次元配列』で検索
          -->
        </tr>
        <tr>
          <th id=resultth>名前</th>
          <td>
            <?php echo $result[0]['name'];?>
          </td>
        </tr>
        <tr>
          <th id=resultth>出身地</th>
          <td>
            <?php echo $pref_array[$result[0]['pref']];?>
          </td>
        </tr>
        <tr>
          <th id=resultth>性別</th>
          <td>
            <?php echo $gender_array[$result[0]['seibetu']];?>
          </td>
        </tr>
        <tr>
          <th id=resultth>年齢</th>
          <td>
            <?php echo $result[0]['age'];?>
          </td>
        </tr>
        <tr>
          <th id=resultth>所属部署</th>
          <td>
            <?php echo $result[0]['section_name'];?>
          </td>
        </tr>
        <tr>
          <th id=resultth>役職</th>
          <td>
            <?php echo $result[0]['grade_name'];?>
          </td>
        </tr>
      </table>
      <br>
      </tr>
      <div id=resultbutton>
        <form action='./entry_update01.php' method='GET'>
          <input type='hidden' value='<?php echo $result[0]['member_ID']; ?>' name='id'>
          <!-- ↑今書いているものがhtmlかphpかちゃんと確認しよう… -->
          <input type='submit' value=' 編集 ' class="btn btn-primary">
        </form>

        <form name='delete' action='./delete01.php' method='POST'>
          <input type='hidden' value='<?php echo $result[0]['member_ID']; ?>' name='member_ID'>
          <input type="button" value=" 削除 " onclick='goDel();' class="btn btn-outline-primary">
        </form>
      </div>

      <!-- 生データ取得 -->
      <!-- <pre>
      <php  var_dump($_POST); ?>
      </pre> -->

    </body>
  </html>
